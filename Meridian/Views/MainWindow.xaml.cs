﻿using Meridian.Helper.Extensions;
using Meridian.ViewModels;
using Meridian.Views.ModalWindows;
using Meridian.Views.UserControls;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Meridian.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            //Clean up Thumbnail Directory
            var thumbDir = Path.Combine(Path.GetTempPath(), "Thumbnails");
            if (Directory.Exists(thumbDir))
                Directory.Delete(thumbDir, true);

            LandingWindow.Instance.SetupModalWindow();
            LandingWindow.Instance.ModalWindow.ShowDialog();
            //InitializeComponent();
        }


        public static void ClearTreeViewSelection(TreeView tview)
        {
            if (tview != null)
                ClearTreeViewItemsControlSelection(tview.Items, tview.ItemContainerGenerator);
        }
        private static void ClearTreeViewItemsControlSelection(ItemCollection ic, ItemContainerGenerator icg)
        {
            if ((ic != null) && (icg != null))
                for (int i = 0; i < ic.Count; i++)
                {
                    // Get the TreeViewItem
                    TreeViewItem tvi = icg.ContainerFromIndex(i) as TreeViewItem;
                    if (tvi != null)
                    {
                        //Recursive call to traverse deeper levels
                        ClearTreeViewItemsControlSelection(tvi.Items, tvi.ItemContainerGenerator);
                        //Deselect the TreeViewItem 
                        tvi.IsSelected = false;
                    }
                }
        }


        private void TabViewControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var treeView = UC_TreeExplorer.Instance.FindName("TreeViewObj") as TreeView;
            ClearTreeViewSelection(treeView);
            //var treeViewSelItem = treeView.SelectedItem;
            //treeView.ContainerFromItem(treeView.SelectedItem).IsSelected = false;

            var selectedTab = TabViewControl.SelectedItem as TabItem;

            if (selectedTab.Content == null)
            {
                var headItemSP = selectedTab.Header as StackPanel;
                var headTB = headItemSP.Children[1] as TextBlock;
                var headTitle = headTB.Text;

                switch(headTitle)
                {
                    case "Nasteri":
                        UC_Nasteri.Instance.DataContext = VM_Nasteri.Default;
                        selectedTab.Content = UC_Nasteri.Instance;
                        break;
                    case "Casatorii":
                        //UC_Casatorii.Instance.DataContext = VM_Casatorii.Default;
                        selectedTab.Content = UC_Casatorii.Instance;
                        break;
                    case "Decese":
                        UC_Decese.Instance.DataContext = VM_Decese.Default;
                        selectedTab.Content = UC_Decese.Instance;
                        break;
                }

            }              
        }
    }
}
