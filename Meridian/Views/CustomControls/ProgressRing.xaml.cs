﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Meridian.UserControls.CustomControls
{
    /// <summary>
    /// Interaction logic for ProgressRing.xaml
    /// </summary>
    public partial class ProgressRing : UserControl
    {
        public DoubleAnimation roateAnimation { get; set; }
        public RotateTransform rotateTransform { get; set; }
        public bool IsBusy
        {
            get { return (bool)GetValue(IsBusyProperty); }
            set { SetValue(IsBusyProperty, value); }
        }
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(ProgressRing), new PropertyMetadata(true, new PropertyChangedCallback(OnIsBusyChanged)));

        private static void OnIsBusyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {

                ProgressRing Bi = (ProgressRing)d;
                Bi.Visibility = Visibility.Hidden;
                Bi.rotateTransform.BeginAnimation(RotateTransform.AngleProperty, null);
            }

            else
            {
                ProgressRing Bi = (ProgressRing)d;
                Bi.Visibility = Visibility.Visible;
                Bi.rotateTransform.BeginAnimation(RotateTransform.AngleProperty, Bi.roateAnimation);
            }
        }

        public SolidColorBrush FillBrush
        {
            get { return (SolidColorBrush)GetValue(FillBrushProperty); }
            set { SetValue(FillBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FillBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FillBrushProperty =
            DependencyProperty.Register("FillBrush", typeof(SolidColorBrush), typeof(ProgressRing), new PropertyMetadata(new SolidColorBrush(Colors.Black)));

        public ProgressRing()
        {
            InitializeComponent();
            roateAnimation = new DoubleAnimation(360, new Duration(TimeSpan.FromSeconds(3)));
            roateAnimation.AutoReverse = false;
            rotateTransform = new RotateTransform();
            grid.RenderTransform = rotateTransform;
            grid.RenderTransformOrigin = new Point(0.5, 0.5);
            roateAnimation.RepeatBehavior = RepeatBehavior.Forever;
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, roateAnimation);
        }
    }
}
