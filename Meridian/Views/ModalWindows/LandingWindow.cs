﻿using Meridian.Helper;
using Meridian.Views.Decorators;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Meridian.Views.ModalWindows
{
    public class LandingWindow
    {
        public Window ModalWindow { get; private set; }
        public static readonly LandingWindow Instance = new LandingWindow();
        public static bool OkEventTriggered { get; set; }

        static LandingWindow()
        {
            new LandingWindow().SetupModalWindow();
        }

        public void SetupModalWindow()
        {
            //Report Dialog popup
            var contentTemplate = (ControlTemplate)App.Current.FindResource("LandPageContent");
            var contentControl = new ContentControl
            {
                Template = contentTemplate,
                Content = ConfigManager.LandPageTitle
            };

            var dpiDecorator = new DpiDecorator();
            dpiDecorator.Child = contentControl;

            var wind = new Window
            {
                Name = "ModalWindow",
                SizeToContent = SizeToContent.WidthAndHeight,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                Content = dpiDecorator
            };

            wind.Closing += Wind_Closing;
            Instance.ModalWindow = wind;

            //Reset OkEventTriggered
            OkEventTriggered = false;
        }

        private void Wind_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //If x button of landing page was pressed close application
            if (!OkEventTriggered)
            {
                Application.Current.Shutdown();
                Environment.Exit(0);
            }                
        }
    }
}
