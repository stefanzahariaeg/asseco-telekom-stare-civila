﻿using System.Windows;
using System.Windows.Controls;

namespace Meridian.Views.ModalWindows
{
    public class CalendarWindow
    {
        public Window ModalWindow { get; private set; }
        public static readonly CalendarWindow Instance = new CalendarWindow();
        static CalendarWindow()
        {
            new CalendarWindow().SetupModalWindow();
        }

        public void SetupModalWindow()
        {
            //Report Dialog popup
            DatePicker dp = new DatePicker
            {
                Style = (Style)App.Current.FindResource("ReportCalendarDialog"),
                Name = "DatePickerControl"
            };

            var wind = new Window
            {
                Name = "ModalWindow",
                SizeToContent = SizeToContent.WidthAndHeight,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                Owner = Application.Current.MainWindow,
                Content = dp
            };

            Instance.ModalWindow = wind;

        }

    }
}

