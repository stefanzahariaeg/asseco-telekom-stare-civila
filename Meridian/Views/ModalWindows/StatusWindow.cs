﻿using System.Windows;
using System.Windows.Controls;

namespace Meridian.Views.ModalWindows
{
    public class StatusWindow
    {
        public Window ModalWindow { get; private set; }
        public static readonly StatusWindow Instance = new StatusWindow();
        static StatusWindow() {
            new StatusWindow().SetupModalWindow();
        }

        public void SetupModalWindow()
        {  
            //Reason Dialog popup
            TextBox tb = new TextBox
            {
                Style = (Style)App.Current.FindResource("ChangeReasonDialog"),
                Name = "TBoxName"
            };

            var wind = new Window
            {
                Name = "ModalWindow",
                SizeToContent = SizeToContent.WidthAndHeight,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                Owner = Application.Current.MainWindow,
                Content = tb
            };

            Instance.ModalWindow = wind;
           
        }
        
    }
}
