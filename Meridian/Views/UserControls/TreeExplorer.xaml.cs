﻿using Meridian.Control.Helper;
using Meridian.Helper;
using Meridian.Helper.Commands;
using Meridian.Helper.Controls;
using Meridian.Helper.Controls.DataGrid;
using Meridian.Helper.Enums;
using Meridian.Helper.Extensions;
using Meridian.Models.NewModels.Casatorii;
using Meridian.Models.NewModels.Decese;
using Meridian.Models.NewModels.Nasteri;
using Meridian.UserControls.CustomControls;
using Meridian.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Meridian.Views.UserControls
{
    /// <summary>
    /// Interaction logic for TreeExplorer.xaml
    /// </summary>
    public partial class UC_TreeExplorer : UserControl
    {
        #region Private Fields
        public static string RootPathFolder { get; set; }

        private static IEnumerable<string> _casatoriiCollection;
        private static IEnumerable<string> _nasteriCollection;
        private static IEnumerable<string> _deceseCollection;

        //Cached xmlCollection - Contains all xml types
        private static IEnumerable<string> XmlCollection
        {
            get
            {
                return TabOperations.CurrentTab switch
                {
                    XSDEnum.Casatorii => _casatoriiCollection,
                    XSDEnum.Nasteri => _nasteriCollection,
                    XSDEnum.Decese => _deceseCollection,
                    _ => throw new System.Exception("Tab Unknown"),
                };               
            }
            set
            {
                switch(TabOperations.CurrentTab)
                {
                    case XSDEnum.Casatorii:
                        _casatoriiCollection = value;
                        break;
                    case XSDEnum.Nasteri:
                        _nasteriCollection = value;
                        break;
                    case XSDEnum.Decese:
                        _deceseCollection = value;
                        break;
                }
            }
        }

        /// <summary>
        /// object node used to model a tree item
        /// </summary>
        private readonly object _dummyNode = null;
        #endregion

        private static UC_TreeExplorer _instance;
        public static UC_TreeExplorer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_TreeExplorer();
                return _instance;
            }
        }

        public UC_TreeExplorer()
        {
            InitializeComponent();
            RenderTreeList();
        }


        /// <summary>
        /// Method used to render Folder Tree View structure
        /// </summary>
        private void RenderTreeList()
        {
            //Get current exec path
            var folderPath = FileUtils.GetExecutingDirectoryPath();
            var folderName = FileUtils.GetExecutingDirectoryName();

            var stackPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };

            //create tree Item head template
            var headImg = new Image
            {
                Width = 20,
                Height = 15,
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/folder.png"))
            };

            stackPanel.Children.Add(headImg);
            stackPanel.Children.Add(new TextBlock() { Text = folderName });

            //Setup root for tree view
            var item = new TreeViewItem
            {
                Header = stackPanel,
                Tag = folderPath,
                FontWeight = FontWeights.Normal
            };

            item.Items.Add(_dummyNode);
            item.Expanded += new RoutedEventHandler(TreeExplorer.Folder_Expanded);
            TreeViewObj.Items.Add(item);
            TreeViewObj.SelectedItemChanged += TreeView_SelectedItemChanged;
        }

        /// <summary>
        /// Event used to change datagrid view based on tree item selection
        /// </summary>
        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //Reset collections on new Tree Selection
            ClearVMCollections();

            //Propagate Tree Selection to Grid View Rendering
            var tree = (TreeView)sender;
            var selectedItem = ((TreeViewItem)tree.SelectedItem);

            if (selectedItem == null)
                return;

            var selectedItemPath = selectedItem.Tag.ToString();
            RootPathFolder = selectedItemPath;

            //Trigger Xml -> Model assignation - Async        
            _ = InitialLoadGridAsync(selectedItemPath);

            //Disable buttons when selecting new folder in tree
            new StatusControl()
                .BtnDisableCommand.Execute(null);
        }

        /// <summary>
        /// Start initial collection load
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        static async Task InitialLoadGridAsync(string rootPath)
        {
            //Get context category
            var xsdVal = TabOperations.CurrentTab;

            //Hide datagrid in current tab
            GridDisplay.HideTable();

            //Initialize progress ring
            var progressRing = ProgressRingHandler.CurrentProgressRing;
            var progressHandler = new ProgressRingHandler(progressRing);
            
            progressHandler.Start();

            //Start async collection initial load
            await Task.Run(() =>
            {
                //Cache collection in static variable
                XmlCollection = FileUtils.GetFileListInFolder(rootPath)
                    .Where(fp => new FileInfo(fp).Extension == ".xml");

                //Filter collection based on document type
                var filteredCollection = XmlCollection
                    .Where(fp => XmlUtils.IsValidXml(fp, XsdMapper.XsdPathMap[xsdVal]));
      
                //Render first 6 entries
                filteredCollection
                    .Take(6)
                    .ToList()
                    .ForEach(filePath =>
                    {
                        new GridDisplay().AssignXmlToModel(filePath, xsdVal);
                    });

                //calculate collection length - async process as it takes a long time
                AsyncCountCollectionTotal(xsdVal, filteredCollection);
            });

            //Stop progress ring
            progressHandler.Stop();

            //Show datagrid in current tab
            GridDisplay.ShowTable();

            //Render entries
            UpdateUI(xsdVal);
        }

        /// <summary>
        /// Load entries between given interval
        /// </summary>
        /// <param name="indexStart"></param>
        /// <param name="intervalLength"></param>
        public static void PopulateGridWithinInterval(int indexStart, int intervalLength)
        {
            var xsdVal = TabOperations.CurrentTab;

            XmlCollection
                .Where(fp => XmlUtils.IsValidXml(fp, XsdMapper.XsdPathMap[xsdVal]))
                .Skip(indexStart)
                .Take(intervalLength)
                .ToList()
                .ForEach(filePath =>
                {
                    new GridDisplay().AssignXmlToModel(filePath, xsdVal);
                });     
        }

        /// <summary>
        /// Method used to clear VM collections when new tree item is selected
        /// </summary>
        public static void ClearVMCollections()
        {
            DataCollection<Nasteri>.asyncDosareLst.Clear();
            DataCollection<Casatorii>.asyncDosareLst.Clear();
            DataCollection<Decese>.asyncDosareLst.Clear();
        }

        /// <summary>
        /// Count filtered collection for corresponding type
        /// </summary>
        /// <param name="xsdEnum"></param>
        /// <param name="filteredCollection"></param>
        private static void AsyncCountCollectionTotal(XSDEnum xsdEnum, IEnumerable<string> filteredCollection)
        {
            Task.Factory.StartNew(() =>
            {
                switch (xsdEnum)
                {
                    case XSDEnum.Casatorii:
                        VM_Casatorii.Default.totalItems = 5;
                        VM_Casatorii.Default.totalItems = filteredCollection.CustomCount();
                        VM_Casatorii.Default.RefreshEntries();
                        break;
                    case XSDEnum.Nasteri:
                        VM_Nasteri.Default.totalItems = 5;
                        VM_Nasteri.Default.totalItems = filteredCollection.CustomCount();
                        VM_Nasteri.Default.RefreshEntries();
                        break;
                    case XSDEnum.Decese:
                        VM_Decese.Default.totalItems = 5;
                        VM_Decese.Default.totalItems = filteredCollection.CustomCount();
                        VM_Decese.Default.RefreshEntries();
                        break;

                }
            }, TaskCreationOptions.AttachedToParent);
        }

        /// <summary>
        /// Updates the corresponding Datagrid with new entries
        /// </summary>
        /// <param name="xsdEnum"></param>
        private static void UpdateUI(XSDEnum xsdEnum)
        {
            switch (xsdEnum)
            {
                case XSDEnum.Casatorii:
                    VM_Casatorii.Default.ResetNavigation();
                    VM_Casatorii.Default.RefreshEntries();
                    break;
                case XSDEnum.Nasteri:
                    VM_Nasteri.Default.ResetNavigation();
                    VM_Nasteri.Default.RefreshEntries();
                    break;
                case XSDEnum.Decese:
                    VM_Decese.Default.ResetNavigation();
                    VM_Decese.Default.RefreshEntries();
                    break;
            }
        }

        private void TreeViewObj_LostFocus(object sender, RoutedEventArgs e)
        {
            var tree = (TreeView)sender;
            UnselectTreeViewItem(tree);
        }

        private void UnselectTreeViewItem(TreeView pTreeView)
        {
            if (pTreeView.SelectedItem == null)
                return;

            if (pTreeView.SelectedItem is TreeViewItem)
            {
                (pTreeView.SelectedItem as TreeViewItem).IsSelected = false;
            }
            else
            {
                TreeViewItem item = pTreeView.ItemContainerGenerator.ContainerFromIndex(0) as TreeViewItem;
                if (item != null)
                {
                    item.IsSelected = true;
                    item.IsSelected = false;
                }
            }
        }
    }
}
