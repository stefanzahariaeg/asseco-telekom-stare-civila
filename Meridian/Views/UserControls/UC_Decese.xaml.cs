﻿using System.Windows.Controls;

namespace Meridian.Views.UserControls
{
    /// <summary>
    /// Interaction logic for UC_Decese.xaml
    /// </summary>
    public partial class UC_Decese : UserControl
    {
        private static UC_Decese _instance;
        public static UC_Decese Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Decese();
                return _instance;
            }
        }
        public UC_Decese()
        {
            InitializeComponent();
        }
    }
}
