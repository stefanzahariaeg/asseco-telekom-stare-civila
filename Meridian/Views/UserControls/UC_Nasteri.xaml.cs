﻿using System.Windows.Controls;

namespace Meridian.Views.UserControls
{
    /// <summary>
    /// Interaction logic for UC_Nasteri.xaml
    /// </summary>
    public partial class UC_Nasteri : UserControl
    {
        private static UC_Nasteri _instance;
        public static UC_Nasteri Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Nasteri();
                return _instance;
            }
        }
        public UC_Nasteri()
        {
            InitializeComponent();          
        }
    }
}
