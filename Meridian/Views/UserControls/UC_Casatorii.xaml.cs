﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Meridian.Views.UserControls
{
    /// <summary>
    /// Interaction logic for UC_Casatorii.xaml
    /// </summary>
    public partial class UC_Casatorii : UserControl
    {
        private static UC_Casatorii _instance;
        public static UC_Casatorii Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new UC_Casatorii();
                return _instance;
            }
        }
        public UC_Casatorii()
        {
            DataContext = ViewModels.VM_Casatorii.Default;
            InitializeComponent();
        }

        private void DG_Casatorii_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;
                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                    if (!dgr.IsMouseOver)
                    {
                        (dgr as DataGridRow).IsSelected = false;
                    }
                }
            }
        }
    }
}
