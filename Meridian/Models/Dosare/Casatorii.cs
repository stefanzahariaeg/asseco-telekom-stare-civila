﻿using Meridian.Helper.Enums;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Meridian.Models.NewModels.Casatorii
{
	[XmlRoot(ElementName = "judet_inregistrare_act")]
	public class Judet_inregistrare_act
	{
		[XmlAttribute(AttributeName = "cod_judet")]
		public string Cod_judet { get; set; }
		[XmlAttribute(AttributeName = "tip_judet")]
		public string Tip_judet { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "uat_inregistrare_act")]
	public class Uat_inregistrare_act
	{
		[XmlAttribute(AttributeName = "cod_uat")]
		public string Cod_uat { get; set; }
		[XmlAttribute(AttributeName = "tip_uat")]
		public string Tip_uat { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "judet_inregistrare_denact")]
	public class Judet_inregistrare_denact
	{
		[XmlAttribute(AttributeName = "cod_judet")]
		public string Cod_judet { get; set; }
		[XmlAttribute(AttributeName = "tip_jud")]
		public string Tip_jud { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "uat_inregistrare_denact")]
	public class Uat_inregistrare_denact
	{
		[XmlAttribute(AttributeName = "cod_uat")]
		public string Cod_uat { get; set; }
		[XmlAttribute(AttributeName = "id_uat")]
		public string Id_uat { get; set; }
		[XmlAttribute(AttributeName = "tip_uat")]
		public string Tip_uat { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "tara")]
	public class Tara
	{
		[XmlAttribute(AttributeName = "cod_tara")]
		public string Cod_tara { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "misiune")]
	public class Misiune
	{
		[XmlAttribute(AttributeName = "cod_misiune")]
		public string Cod_misiune { get; set; }
		[XmlAttribute(AttributeName = "id_reg_mis")]
		public string Id_reg_mis { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "act")]
	public class Act
	{
		[XmlElement(ElementName = "judet_inregistrare_act")]
		public Judet_inregistrare_act Judet_inregistrare_act { get; set; }
		[XmlElement(ElementName = "uat_inregistrare_act")]
		public Uat_inregistrare_act Uat_inregistrare_act { get; set; }
		[XmlElement(ElementName = "judet_inregistrare_denact")]
		public Judet_inregistrare_denact Judet_inregistrare_denact { get; set; }
		[XmlElement(ElementName = "uat_inregistrare_denact")]
		public Uat_inregistrare_denact Uat_inregistrare_denact { get; set; }
		[XmlElement(ElementName = "tara")]
		public Tara Tara { get; set; }
		[XmlElement(ElementName = "misiune")]
		public Misiune Misiune { get; set; }
		[XmlElement(ElementName = "nr_act")]
		public string Nr_act { get; set; }
		[XmlElement(ElementName = "data_inregistrare_act")]
		public string Data_inregistrare_act { get; set; }
		[XmlElement(ElementName = "an_inregistrare_act")]
		public string An_inregistrare_act { get; set; }
		[XmlElement(ElementName = "tip_inregistrare")]
		public string Tip_inregistrare { get; set; }
		[XmlAttribute(AttributeName = "tip")]
		public string Tip { get; set; }
	}

	[XmlRoot(ElementName = "judet")]
	public class Judet
	{
		[XmlAttribute(AttributeName = "tip_judet")]
		public string Tip_judet { get; set; }
		[XmlAttribute(AttributeName = "cod_judet")]
		public string Cod_judet { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "localitate")]
	public class Localitate
	{
		[XmlAttribute(AttributeName = "id_loc")]
		public string Id_loc { get; set; }
		[XmlAttribute(AttributeName = "cod_loc")]
		public string Cod_loc { get; set; }
		[XmlAttribute(AttributeName = "tip_loc")]
		public string Tip_loc { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "loc_nastere")]
	public class Loc_nastere
	{
		[XmlElement(ElementName = "judet")]
		public Judet Judet { get; set; }
		[XmlElement(ElementName = "localitate")]
		public Localitate Localitate { get; set; }
		[XmlElement(ElementName = "tara")]
		public Tara Tara { get; set; }
	}

	[XmlRoot(ElementName = "tata")]
	public class Tata
	{
		[XmlElement(ElementName = "nume")]
		public string Nume { get; set; }
		[XmlElement(ElementName = "prenume")]
		public string Prenume { get; set; }
	}

	[XmlRoot(ElementName = "mama")]
	public class Mama
	{
		[XmlElement(ElementName = "nume")]
		public string Nume { get; set; }
		[XmlElement(ElementName = "prenume")]
		public string Prenume { get; set; }
	}

	[XmlRoot(ElementName = "sot")]
	public class Sot
	{
		[XmlElement(ElementName = "cnp")]
		public string Cnp { get; set; }
		[XmlElement(ElementName = "numeprenume_inainte_casatorie")]
		public string Numeprenume_inainte_casatorie { get; set; }
		[XmlElement(ElementName = "nume_inainte_casatorie")]
		public string Nume_inainte_casatorie { get; set; }
		[XmlElement(ElementName = "prenume")]
		public string Prenume { get; set; }
		[XmlElement(ElementName = "an_nastere")]
		public string An_nastere { get; set; }
		[XmlElement(ElementName = "data_nastere")]
		public string Data_nastere { get; set; }
		[XmlElement(ElementName = "varsta")]
		public string Varsta { get; set; }
		[XmlElement(ElementName = "loc_nastere")]
		public Loc_nastere Loc_nastere { get; set; }
		[XmlElement(ElementName = "tata")]
		public Tata Tata { get; set; }
		[XmlElement(ElementName = "mama")]
		public Mama Mama { get; set; }
		[XmlElement(ElementName = "nume_dupa_casatorie")]
		public string Nume_dupa_casatorie { get; set; }
		[XmlElement(ElementName = "cetatenie")]
		public string Cetatenie { get; set; }
	}

	[XmlRoot(ElementName = "sotie")]
	public class Sotie
	{
		[XmlElement(ElementName = "cnp")]
		public string Cnp { get; set; }
		[XmlElement(ElementName = "numeprenume_inainte_casatorie")]
		public string Numeprenume_inainte_casatorie { get; set; }
		[XmlElement(ElementName = "nume_inainte_casatorie")]
		public string Nume_inainte_casatorie { get; set; }
		[XmlElement(ElementName = "prenume")]
		public string Prenume { get; set; }
		[XmlElement(ElementName = "an_nastere")]
		public string An_nastere { get; set; }
		[XmlElement(ElementName = "data_nastere")]
		public string Data_nastere { get; set; }
		[XmlElement(ElementName = "varsta")]
		public string Varsta { get; set; }
		[XmlElement(ElementName = "loc_nastere")]
		public Loc_nastere Loc_nastere { get; set; }
		[XmlElement(ElementName = "tata")]
		public Tata Tata { get; set; }
		[XmlElement(ElementName = "mama")]
		public Mama Mama { get; set; }
		[XmlElement(ElementName = "nume_dupa_casatorie")]
		public string Nume_dupa_casatorie { get; set; }
		[XmlElement(ElementName = "cetatenie")]
		public string Cetatenie { get; set; }
	}

	[XmlRoot(ElementName = "infoDigitizare")]
	public class InfoDigitizare
	{
		[XmlElement(ElementName = "id_registru")]
		public string Id_registru { get; set; }
		[XmlElement(ElementName = "nr_mentiuni")]
		public string Nr_mentiuni { get; set; }
		[XmlElement(ElementName = "data_scanare")]
		public string Data_scanare { get; set; }
		[XmlElement(ElementName = "nr_set_registre")]
		public string Nr_set_registre { get; set; }
		[XmlElement(ElementName = "jpg")]
		public string Jpg { get; set; }
		[XmlElement(ElementName = "pdf")]
		public string Pdf { get; set; }
		[XmlElement(ElementName = "observatii")]
		public string Observatii { get; set; }
	}

	[XmlRoot(ElementName = "document")]
	public class Casatorii : INotifyPropertyChanged
	{
		[XmlElement(ElementName = "act")]
		public Act Act { get; set; }
		[XmlElement(ElementName = "sot")]
		public Sot Sot { get; set; }
		[XmlElement(ElementName = "sotie")]
		public Sotie Sotie { get; set; }
		[XmlElement(ElementName = "infoDigitizare")]
		public InfoDigitizare InfoDigitizare { get; set; }
		[XmlAttribute(AttributeName = "ID")]
		public string ID { get; set; }

		private DosarStatusEnum _status;
		public DosarStatusEnum Status
		{
			get { return _status; }
			set
			{
				_status = value;
				RaisePropertyChanged("Status");
			}
		}
		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
