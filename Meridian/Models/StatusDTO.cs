﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Meridian.Models.StatusDTO
{
	[XmlRoot(ElementName = "document")]
	public class StatusDocument
	{
		[XmlElement(ElementName = "Status")]
		public string Status { get; set; }

		[XmlElement(ElementName = "Data")]
		public string Data { get; set; }

		[XmlElement(ElementName = "Motiv")]
		public string Motiv { get; set; }

		[XmlAttribute(AttributeName = "ID")]
		public string ID { get; set; }
	}

	[XmlRoot(ElementName = "documents")]
	public class StatusDocuments
	{
		[XmlElement(ElementName = "document")]
		public List<StatusDocument> Document { get; set; }
	}
}
