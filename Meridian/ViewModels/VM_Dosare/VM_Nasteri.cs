﻿using Meridian.Helper.Controls.DataGrid;
using Meridian.Models.NewModels.Nasteri;
using System.Collections.ObjectModel;

namespace Meridian.ViewModels
{
    public class VM_Nasteri : VM_Base
    {
        #region Private Fields
        private ObservableCollection<Nasteri> nasteri;
        #endregion

        #region Self Registration
        public static VM_Nasteri Default
        {
            get
            {
                if (_instance == null)
                    _instance = new VM_Nasteri();
                return _instance;
            }
        }
        private static VM_Nasteri _instance;

        public VM_Nasteri()
        {
            Nasteri = new ObservableCollection<Nasteri>();

        }
        #endregion

        /// <summary>
        /// The list of documents in the current page.
        /// </summary>
        public ObservableCollection<Nasteri> Nasteri
        {
            get
            {
                return nasteri;
            }
            set
            {
                if (object.ReferenceEquals(nasteri, value) != true)
                {
                    nasteri = value;
                    NotifyPropertyChanged("Nasteri");
                }
            }
        }
        /// <summary>

        #region Refresh Navigation
        /// <summary>
        /// Refreshes the list of documents. Called by navigation commands.
        /// </summary>
        public override void RefreshEntries()
        {
            Nasteri = DataCollection<Nasteri>.GeDocuments(itemCount);

            NotifyPropertyChanged("Start");
            NotifyPropertyChanged("End");
            NotifyPropertyChanged("TotalItems");
            NotifyPropertyChanged("PageNr");
        }
        #endregion
    }
}
