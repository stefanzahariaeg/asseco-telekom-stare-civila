﻿using Meridian.Commands;
using Meridian.Helper;
using Meridian.Helper.Controls.Thumbnail.ImageResize;
using Meridian.Views.Decorators;
using Meridian.Views.UserControls;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.ViewModels
{
    public class VM_Base : INotifyPropertyChanged
    {
        #region Private Fields
        private ICommand _keyPressedCommand;
        private Uri _imgUri;
        private int start = 0;
        private ICommand previousCommand;
        private ICommand nextCommand;
        #endregion

        #region Public Fields
        protected int itemCount = 5;
        public int totalItems = 0;
        #endregion

        #region Public Members
        /// Gets the index of the first item in the products list.
        /// </summary>
        public int Start { get { return start + 1; } }

        /// <summary>
        /// Gets the index of the last item in the products list.
        /// </summary>
        public int End { get { return start + itemCount < totalItems ? start + itemCount : totalItems; } }

        /// <summary>
        /// Gets the current page number
        /// </summary>
        public int PageNr
        {
            get
            {
                var pageNr = End / itemCount;
                if (End % itemCount != 0)
                    pageNr++;

                return pageNr;
            }
        }

        /// <summary>
        /// Gets the total number of xml documents in the collection
        /// </summary>
        public int TotalItems
        {
            get
            {
                var total = totalItems / itemCount;
                if (totalItems % itemCount != 0)
                    total++;

                return total;
            }
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Key Commands
        public ICommand KeyPressedCommand
        {
            get
            {
                return _keyPressedCommand ?? (_keyPressedCommand = new RelayCommand(
                   x =>
                   {
                       if (x != null)
                       {
                           if (!new FileInfo(x.ToString()).Exists)
                               return;

                           var dpiDecorator = new DpiDecorator();
                           var browserChild = new WebBrowser();
                          
                           var filePath = x.ToString();

                           var tempDirPath = FileUtils.GetTempPath(filePath);
                           var fileOutName = Path.ChangeExtension(Path.GetFileName(filePath), ".jpg");
                           var outputFilePath = Path.Combine(tempDirPath, fileOutName);

                           //fileOutBase is used to keep a base image for ImageResizing (using the same image for subsequent resizing causes quality loss)
                           var fileOutBase = Path.ChangeExtension(Path.GetFileNameWithoutExtension(filePath) + "_base", ".jpg");
                           var filePathBase = Path.Combine(tempDirPath, fileOutBase);

                           var absPath = Path.GetFullPath(filePath);

                           if (filePath.Contains(".jpg"))
                           {
                               File.Copy(x.ToString(), outputFilePath, true);
                               if (!File.Exists(filePathBase))
                                   File.Copy(x.ToString(), filePathBase);

                               _imgUri = new Uri("file:///" + outputFilePath);
                               browserChild.Navigate(_imgUri);

                               browserChild.SizeChanged += BrowserChild_SizeChanged;
                           }
                           else if (filePath.Contains(".pdf"))
                           {
                               _imgUri = new Uri("file:///" + x.ToString());
                               browserChild.Navigate(_imgUri);
                           }

                           dpiDecorator.Child = browserChild;

                           var screenWidth = SystemParameters.WorkArea.Width;
                           var screenHeight = SystemParameters.WorkArea.Height;
                           var modalWidth = (int)Math.Round(screenWidth / 2);
                           var modalHeight = (int)Math.Round(screenHeight * 0.75);

                           var wind = new Window
                           {
                               Width = modalWidth,
                               Height = modalHeight,
                               HorizontalAlignment = HorizontalAlignment.Center,
                               VerticalAlignment = VerticalAlignment.Center,
                               WindowStartupLocation = WindowStartupLocation.CenterScreen,
                               Owner = Application.Current.MainWindow,
                               Content = dpiDecorator
                           };

                           wind.Closed += Wind_Closed;
                           wind.ShowDialog();
                       }

                   }));
            }

        }
        #endregion

        #region Navigation Commands

        /// <summary>
        /// Gets the command for moving to the previous page of products.
        /// </summary>
        public ICommand PreviousCommand
        {
            get
            {
                if (previousCommand == null)
                {
                    previousCommand = new RelayCommand
                    (
                        param =>
                        {
                            //Move start to previous page
                            start -= itemCount;
                            //Clear Collection so we don't stack up data (always use collection size = itemCount size)
                            UC_TreeExplorer.ClearVMCollections();
                            //Repopulate collection with previous 5 entries
                            UC_TreeExplorer.PopulateGridWithinInterval(start, itemCount);
                            //Update UI
                            RefreshEntries();
                        },
                        param =>
                        {
                            return start - itemCount >= 0 ? true : false;
                        }
                    );
                }
                return previousCommand;
            }
        }


        /// <summary>
        /// Gets the command for moving to the next page of products.
        /// </summary>
        public ICommand NextCommand
        {
            get
            {
                if (nextCommand == null)
                {
                    nextCommand = new RelayCommand
                    (
                        param =>
                        {
                            //Move start to next page
                            start += itemCount;
                            //Clear Collection so we don't stack up data (always use collection size = itemCount size)
                            UC_TreeExplorer.ClearVMCollections();
                            //Repopulate collection with next 5 entries
                            UC_TreeExplorer.PopulateGridWithinInterval(start, itemCount);
                            //Update UI
                            RefreshEntries();
                        },
                        param =>
                        {
                            return start + itemCount < totalItems ? true : false;
                        }
                    );
                }
                return nextCommand;
            }
        }
        #endregion

        /// <summary>
        /// After close event - keep focus on datagrid row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wind_Closed(object sender, EventArgs e)
        {
            var tabControl = App.Current.MainWindow.FindName("TabViewControl") as TabControl;
            var tabItem = tabControl.SelectedItem as TabItem;
            var uc = tabItem.Content as UserControl;

            if (uc.Content is DataGrid dg && dg.SelectedCells != null && dg.SelectedCells.Count > 0)
            {
                var selCell = dg.SelectedCells.First();
                dg.CurrentCell = selCell;
            }
        }

        /// <summary>
        /// Resize image to fit WebBrowser Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BrowserChild_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var browser = sender as WebBrowser;
            var width = Convert.ToInt32(browser.ActualWidth);
            var height = Convert.ToInt32(browser.ActualHeight);

            var url = _imgUri;

            //fileOutBase is used to keep a base image for ImageResizing (using the same image for subsequent resizing causes quality loss)
            var filePath = url.LocalPath;
            var tempDirPath = FileUtils.GetTempPath(filePath);
            var fileOutBase = Path.ChangeExtension(Path.GetFileNameWithoutExtension(filePath) + "_base", ".jpg");
            var filePathBase = Path.Combine(tempDirPath, fileOutBase);

            //new ImageResizing(filePathBase)
            //    .Resize(width-5, height)
            //    .Quality(100)
            //    .Save(url.LocalPath); // Calling save will dispose

            ImageHandler.Resize(filePathBase, url.LocalPath, width - 30, height, 300);

            browser.Navigate(url);
        }

        #region Refresh Navigation
        /// <summary>
        /// Refreshes the list of documents. Called by navigation commands.
        /// </summary>
        public virtual void RefreshEntries()
        {
            NotifyPropertyChanged("Start");
            NotifyPropertyChanged("End");
            NotifyPropertyChanged("TotalItems");
            NotifyPropertyChanged("PageNr");
        }

        /// <summary>
        /// Refresh Total Items counter
        /// </summary>
        public void RefreshTotalItems()
        {
            NotifyPropertyChanged("TotalItems");
        }

        /// <summary>
        /// Resets pagination startIndex
        /// </summary>
        public void ResetNavigation()
        {
            start = 0;
        }
        #endregion

        /// <summary>
        /// Notifies subscribers of changed properties.
        /// </summary>
        /// <param name="propertyName">Name of the changed property.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
