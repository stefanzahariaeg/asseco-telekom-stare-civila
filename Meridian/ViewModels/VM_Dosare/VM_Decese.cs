﻿using Meridian.Helper.Controls.DataGrid;
using Meridian.Models.NewModels.Decese;
using System.Collections.ObjectModel;

namespace Meridian.ViewModels
{
    public class VM_Decese : VM_Base
    {
        #region Private Fields
        private ObservableCollection<Decese> decese;
        #endregion

        #region Self Registration
        public static VM_Decese Default
        {
            get
            {
                if (_instance == null)
                    _instance = new VM_Decese();
                return _instance;
            }
        }
        private static VM_Decese _instance;

        public VM_Decese()
        {
            Decese = new ObservableCollection<Decese>();
        }
        #endregion

        /// <summary>
        /// The list of documents in the current page.
        /// </summary>
        public ObservableCollection<Decese> Decese
        {
            get
            {
                return decese;
            }
            set
            {
                if (object.ReferenceEquals(decese, value) != true)
                {
                    decese = value;
                    NotifyPropertyChanged("Decese");
                }
            }
        }
        /// <summary>

        #region Refresh Navigation
        /// <summary>
        /// Refreshes the list of documents. Called by navigation commands.
        /// </summary>
        public override void RefreshEntries()
        {
            Decese = DataCollection<Decese>.GeDocuments(itemCount);

            NotifyPropertyChanged("Start");
            NotifyPropertyChanged("End");
            NotifyPropertyChanged("TotalItems");
            NotifyPropertyChanged("PageNr");
        }
        #endregion
    }
}
