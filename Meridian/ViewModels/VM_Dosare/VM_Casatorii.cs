﻿using Meridian.Helper.Controls.DataGrid;
using Meridian.Models.NewModels.Casatorii;
using System.Collections.ObjectModel;

namespace Meridian.ViewModels
{
    public class VM_Casatorii : VM_Base
    {
        #region Private Fields
        private ObservableCollection<Casatorii> casatorii;
        #endregion

        #region Self Registration
        public static VM_Casatorii Default
        {
            get
            {
                if (_instance == null)
                    _instance = new VM_Casatorii();
                return _instance;
            }
        }
        private static VM_Casatorii _instance;
        #endregion

        public VM_Casatorii()
        {
            RefreshEntries();
        }

        /// <summary>
        /// The list of documents in the current page.
        /// </summary>
        public ObservableCollection<Casatorii> Casatorii
        {
            get
            {
                return casatorii;
            }
            set
            {
                if (object.ReferenceEquals(casatorii, value) != true)
                {
                    casatorii = value;
                    NotifyPropertyChanged("Casatorii");
                }
            }
        }
        /// <summary>

        #region Refresh Navigation
        /// <summary>
        /// Refreshes the list of documents. Called by navigation commands.
        /// </summary>
        public override void RefreshEntries()
        {
            Casatorii = DataCollection<Casatorii>.GeDocuments(itemCount);

            NotifyPropertyChanged("Start");
            NotifyPropertyChanged("End");
            NotifyPropertyChanged("TotalItems");
            NotifyPropertyChanged("PageNr");
        }
        #endregion
    }
}