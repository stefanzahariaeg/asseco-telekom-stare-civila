﻿using Meridian.UserControls.CustomControls;
using Meridian.Views.UserControls;
using System.Windows;

namespace Meridian.Helper.Controls
{
    public class ProgressRingHandler
    {
        private readonly ProgressRing _progressRing;
        public static ProgressRing CurrentProgressRing
        {
            get
            {
                return TabOperations.CurrentTab switch
                {
                    Enums.XSDEnum.Casatorii => UC_Casatorii.Instance.FindName("progressRing") as ProgressRing,
                    Enums.XSDEnum.Nasteri => UC_Nasteri.Instance.FindName("progressRing") as ProgressRing,
                    Enums.XSDEnum.Decese => UC_Decese.Instance.FindName("progressRing") as ProgressRing,
                    _ => throw new System.Exception("Tab Unknown"),
                };
            }
        }

        public ProgressRingHandler() { }
        public ProgressRingHandler(ProgressRing progressRing) 
        {
            _progressRing = progressRing; 
        }

        public void Start()
        {
            _progressRing.Visibility = Visibility.Visible;
            _progressRing.IsBusy = true;
        }

        public void Stop()
        {
            _progressRing.IsBusy = false;
            _progressRing.Visibility = Visibility.Hidden;
        }
    }
}
