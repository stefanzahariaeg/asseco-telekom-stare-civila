﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Meridian.Helper
{
    public static class TreeExplorer
    {
        private static readonly object _dummyNode = null;

        public static void Folder_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item.Items.Count == 1 && item.Items[0] == _dummyNode)
            {
                    item.Items.Clear();

                    foreach (string dir in Directory.GetDirectories(item.Tag as string))
                    {
                        var dirInfo = new DirectoryInfo(dir);

                        //Ignore hidden folders in tree structure
                        if ((dirInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                            continue;

                        var stackPanel = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };

                        //create tree Item head template
                        var headImg = new Image
                        {
                            Width = 20,
                            Height = 15,
                            Source = new BitmapImage(new Uri("pack://application:,,,/Images/folder.png"))
                        };

                        stackPanel.Children.Add(headImg);
                        stackPanel.Children.Add(new TextBlock() { Text = dirInfo.Name });

                        TreeViewItem subitem = new TreeViewItem
                        {
                            Header = stackPanel,
                            Tag = dir,
                            FontWeight = FontWeights.Normal
                        };

                        //don't add expander to last child folder 
                        var isLastChild = new DirectoryInfo(dir).GetDirectories().Length == 0;

                        if (!isLastChild)
                            subitem.Items.Add(_dummyNode);

                        subitem.Expanded += Folder_Expanded;
                        item.Items.Add(subitem);

                    }
            }
        }
    }
}
