﻿using System.IO;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;

namespace Meridian.Control.Helper.TreeView
{
    class XmlTreeMapper
    {
        private string sourceXmlFile;
        private XDocument xmlData;

        public XmlTreeMapper(string xmlFilePath)
        {
            sourceXmlFile = xmlFilePath;
        }

        private void BuildNodes(TreeViewItem treeNode, XElement element)
        {

            string attributes = "";
            if (element.HasAttributes)
            {
                foreach (var att in element.Attributes())
                {
                    attributes += " " + att.Name + " = " + att.Value;
                }
            }

            TreeViewItem childTreeNode = new TreeViewItem
            {
                Header = element.Name.LocalName + attributes,
                IsExpanded = true
            };
            if (element.HasElements)
            {
                foreach (XElement childElement in element.Elements())
                {
                    BuildNodes(childTreeNode, childElement);
                }
            }
            else
            {
                TreeViewItem childTreeNodeText = new TreeViewItem
                {
                    Header = element.Value,
                    IsExpanded = true
                };
                childTreeNode.Items.Add(childTreeNodeText);
            }

            treeNode.Items.Add(childTreeNode);
        }



        public void LoadXml(System.Windows.Controls.TreeView treeview)
        {
            try
            {
                if (sourceXmlFile != null)
                {
                    xmlData = XDocument.Load(sourceXmlFile, LoadOptions.None);
                    if (xmlData == null)
                    {
                        throw new XmlException("Cannot load Xml document from file : " + sourceXmlFile);
                    }
                    else
                    {
                        TreeViewItem treeNode = new TreeViewItem
                        {
                            Header = new FileInfo(sourceXmlFile).Name,
                            IsExpanded = true
                        };


                        BuildNodes(treeNode, xmlData.Root);
                        treeview.Items.Add(treeNode);
                    }
                }
                else
                {
                    throw new IOException("Xml file is not set correctly.");
                }
            }
            catch
            {
                //log
            }
        }

    }
}
