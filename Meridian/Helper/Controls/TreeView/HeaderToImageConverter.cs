﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Meridian.Converter
{
    [ValueConversion(typeof(string), typeof(bool))]
    public class HeaderToImageConverter : IValueConverter
    {
        public static HeaderToImageConverter Instance = new HeaderToImageConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var path = value as string;
            // this is a diskdrive

            if (Directory.Exists(path))
            {
                DirectoryInfo d = new DirectoryInfo(path);
                if (d.Parent == null)
                {
                    Uri uri = new Uri("pack://application:,,,/Images/diskdrive.png");
                    BitmapImage source = new BitmapImage(uri);
                    return source;
                }
                else
                {
                    //maybe this is a directory or maybe this is a file. How do I know it?
                    Uri uri = new Uri("pack://application:,,,/Images/folder.png");
                    //Uri uri = new Uri("pack://application:,,,/Images/file.png");
                    BitmapImage source = new BitmapImage(uri);
                    return source;
                }

            }
            else
            {
                Uri uri = new Uri("pack://application:,,,/Images/diskdrive.png");
                BitmapImage source = new BitmapImage(uri);
                return source;
            }
            //else if (File.Exists(path))
            //{
            //    FileInfo finfo = new FileInfo(path);
            //    string fileExtension = finfo.Extension;
            //    Uri uri = null;
            //
            //    if (fileExtension == ".pdf")
            //        uri = new Uri("pack://application:,,,/Images/pdfIcon.png");
            //    else if (fileExtension == ".xml")
            //        uri = new Uri("pack://application:,,,/Images/xmlFile.png");
            //    else
            //        uri = new Uri("pack://application:,,,/Images/genericFile.png");
            //
            //    BitmapImage source = new BitmapImage(uri);
            //    return source;
            //}

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }
    }
}
