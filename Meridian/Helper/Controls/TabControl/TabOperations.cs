﻿using Meridian.Helper.Enums;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Meridian.Helper.Controls
{
    public class TabOperations
    {
        /// <summary>
        /// Retrieve current document category
        /// </summary>
        /// <returns></returns>
        public static XSDEnum CurrentTab => Application.Current.Dispatcher.Invoke(new Func<XSDEnum>(() =>
        {
            var tabControl = App.Current.MainWindow.FindName("TabViewControl") as TabControl;
            var selectedTab = tabControl.SelectedItem as TabItem;
            var headItemSP = selectedTab.Header as StackPanel;
            var headTB = headItemSP.Children[1] as TextBlock;
            var headTitle = headTB.Text;
            var xsdVal = (XSDEnum)Enum.Parse(typeof(XSDEnum), headTitle);

            return xsdVal;
        }));
    }
}
