﻿using Meridian.Helper;
using Meridian.Helper.Extensions;
using Meridian.Helper.FileCaster;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Meridian.Control.Helper.Thumbnail
{
    public interface ISample
    {
        string Start(string filePath);
    }

    public class RasterizerCropSample : ISample
    {
        public string Start(string filePath)
        {
            var outputPath = FileUtils.GetTempPath(filePath);

            Directory.CreateDirectory(outputPath);

            var fileOutName = Path.ChangeExtension(Path.GetFileName(filePath), ".jpg");
            var tmpFileOutName = Path.GetFileNameWithoutExtension(filePath).AppendTimeStamp() + ".jpg";

            var tmpOutputFilePath = Path.Combine(outputPath, tmpFileOutName);
            var outputFilePath = Path.Combine(outputPath, fileOutName);

            Image img = new PdfToThumb().Cast(filePath);

            //check null
            if (img is null)
                return null;

            //Save in tmp location
            img.Save(tmpOutputFilePath, ImageFormat.Jpeg);
            img.Save(outputFilePath, ImageFormat.Jpeg);

            return tmpOutputFilePath;   
        }
    }
}
