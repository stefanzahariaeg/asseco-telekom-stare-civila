﻿using System.Threading.Tasks;
using System.Windows.Media;

namespace Meridian.Control.Helper.Thumbnail
{
    public class FileManagement
    {
        public string CreateThumbnail(string filePath)
        {
            RasterizerCropSample thumbnailCreate = new RasterizerCropSample();
            return thumbnailCreate.Start(filePath);
        }
    }
}
