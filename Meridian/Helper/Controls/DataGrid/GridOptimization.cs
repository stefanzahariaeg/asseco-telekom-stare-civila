﻿using System;
using System.Linq;
using System.Windows.Controls;

namespace Meridian.Helper
{
    public class GridOptimization
    {
        public static void ResizeTable(DataGrid listView)
        {
            //Fit table to listview horizontal space
            var remainingSpace = listView.ActualWidth;
            var columnList = (listView as DataGrid).Columns;

            if (remainingSpace > 0)
            {
                var remWidth = Math.Ceiling(remainingSpace / columnList.Count);
                columnList
                    .ToList()
                    .ForEach(column => column.Width = remWidth);
            }
        }

        public static void ResizeTableCustom(DataGrid listView)
        {
            ResizeTable(listView);
            //Fit last column to its text content
            var columnList = (listView as DataGrid).Columns;
            columnList[^1].Width = 0;
            columnList[^1].Width = double.NaN;
            columnList[3].Width = 0;
            columnList[3].Width = double.NaN;
            columnList[5].Width = 0;
            columnList[5].Width = double.NaN;
            columnList[7].Width = 0;
            columnList[7].Width = double.NaN;
        }
    }
}
