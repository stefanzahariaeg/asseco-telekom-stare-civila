﻿using Meridian.Helper.Extensions;
using System.Collections.ObjectModel;

namespace Meridian.Helper.Controls.DataGrid
{
    public class DataCollection<T>
    {
        public static AsyncObservableCollection<T> asyncDosareLst = new AsyncObservableCollection<T>();

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="itemCount">Number of products that is requested to be returned.</param>
        /// <returns>List of documents.</returns>
        public static ObservableCollection<T> GeDocuments(int itemCount)
        {
            ObservableCollection<T> filteredDocuments = new ObservableCollection<T>();

            //for (int i = start; i < start + itemCount && i < totalItems; i++)
            //{
            //    filteredProducts.Add(CasatoriiLst[i]);
            //}

            for (int i = 0; i <= itemCount && i < asyncDosareLst.Count; i++)
            {
                filteredDocuments.Add(asyncDosareLst[i]);
            }

            return filteredDocuments;
        }
    }
}
