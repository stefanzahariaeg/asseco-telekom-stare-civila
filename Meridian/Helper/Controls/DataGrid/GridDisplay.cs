﻿using Meridian.Helper;
using Meridian.Helper.Controls;
using Meridian.Helper.Controls.DataGrid;
using Meridian.Helper.Enums;
using Meridian.Helper.Extensions;
using Meridian.Models.NewModels.Casatorii;
using Meridian.Models.NewModels.Decese;
using Meridian.Models.NewModels.Nasteri;
using Meridian.Views.UserControls;
using System.Linq;
using System.Windows.Controls;

namespace Meridian.Control.Helper
{
    public class GridDisplay
    {
        //Validates input document to available xsd types
        public void AssignXmlToModel(string filePath, XSDEnum xsdEnumVal)
        {
            if (xsdEnumVal == XSDEnum.Nasteri)
                PopulateGrid(filePath, DataCollection<Nasteri>.asyncDosareLst);
            else if (xsdEnumVal == XSDEnum.Casatorii)
                PopulateGrid(filePath, DataCollection<Casatorii>.asyncDosareLst);
            else if (xsdEnumVal == XSDEnum.Decese)
                PopulateGrid(filePath, DataCollection<Decese>.asyncDosareLst);
        }

        private void PopulateGrid<T>(string filePath, AsyncObservableCollection<T> observableCollection)
        {
            var xmlList = XmlUtils.InjectXMLCollection<T>(filePath).ToList();

            xmlList
                .ForEach(item =>
                {
                    observableCollection.Add(item);                   
                });
        }

        public static void HideTable()
        {
            var tabCategory = TabOperations.CurrentTab;
            DataGrid dg;

            switch (tabCategory)
            {
                case XSDEnum.Casatorii:
                    dg = UC_Casatorii.Instance.FindName("DG_Casatorii") as DataGrid;
                    dg.IsHitTestVisible = false;
                    break;
                case XSDEnum.Nasteri:
                    dg = UC_Nasteri.Instance.FindName("DG_Nasteri") as DataGrid;
                    dg.IsHitTestVisible = false;
                    break;
                case XSDEnum.Decese:
                    dg = UC_Decese.Instance.FindName("DG_Decese") as DataGrid;
                    dg.IsHitTestVisible = false;
                    break;
            }
        }

        public static void ShowTable()
        {
            var tabCategory = TabOperations.CurrentTab;
            DataGrid dg;

            switch (tabCategory)
            {
                case XSDEnum.Casatorii:
                    dg = UC_Casatorii.Instance.FindName("DG_Casatorii") as DataGrid;
                    dg.IsHitTestVisible = true;
                    break;
                case XSDEnum.Nasteri:
                    dg = UC_Nasteri.Instance.FindName("DG_Nasteri") as DataGrid;
                    dg.IsHitTestVisible = true;
                    break;
                case XSDEnum.Decese:
                    dg = UC_Decese.Instance.FindName("DG_Decese") as DataGrid;
                    dg.IsHitTestVisible = true;
                    break;
            }
        }
    }
}
