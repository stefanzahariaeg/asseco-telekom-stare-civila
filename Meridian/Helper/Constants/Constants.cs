﻿namespace Meridian.Helper
{
    public static class Constants
    {
        public const string FisierStatusDosare = "StatusDosare.xml";
        public const string FisierRaportTxt = "Raport.txt";
        public const string FisierRaportPdf = "Raport.pdf";
    }
}
