﻿using Meridian.Helper.Enums;
using Meridian.Helper.Handlers;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Meridian.Helper.Converters
{
    public class EnumToImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var IsDisconnected = new DisconnectedHandler().IsItemDisconnected(value);
            if (IsDisconnected)
                return null;

            DosarStatusEnum statusDosar = (DosarStatusEnum)Enum.Parse(typeof(DosarStatusEnum), value.ToString());
            object imageSource = null;
            BitmapImage bmpImage = null;

            if (statusDosar == DosarStatusEnum.Verificare)
                bmpImage = new BitmapImage(new Uri("pack://application:,,,/Images/Verificat.png"));
            else if (statusDosar == DosarStatusEnum.Respingere)
                bmpImage = new BitmapImage(new Uri("pack://application:,,,/Images/Respins.png"));
            else if (statusDosar == DosarStatusEnum.Reverificare)
                bmpImage = new BitmapImage(new Uri("pack://application:,,,/Images/ReVerified.png"));

            //if thumbnail creation succeded convert new image path to image source
            if (bmpImage != null)
                imageSource = bmpImage;
            
            return imageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
