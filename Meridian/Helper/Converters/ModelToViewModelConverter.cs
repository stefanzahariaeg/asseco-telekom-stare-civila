﻿using Meridian.Models.NewModels.Casatorii;
using Meridian.Models.NewModels.Decese;
using Meridian.Models.NewModels.Nasteri;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace Meridian.Helper.Converters
{
    public class ModelToViewModelConverter : IValueConverter
    {
        public object Convert(object o, Type type, object parameter, CultureInfo culture)
        {
            if (o == null)
                return null;

            if (o.GetType() == typeof(Nasteri))
            {
                var obsColl = new ObservableCollection<Nasteri>
                {
                    o as Nasteri
                };
                return obsColl;
            }  
            else if (o.GetType() == typeof(Casatorii))
            {
                var obsColl = new ObservableCollection<Casatorii>
                {
                    o as Casatorii
                };
                return obsColl;
            }
            else if (o.GetType() == typeof(Decese))
            {
                var obsColl = new ObservableCollection<Decese>
                {
                    o as Decese
                };
                return obsColl;
            }

            throw new Exception("Model: " + o.GetType().ToString() + " is not of known types");
        }

        public object ConvertBack(object o, Type type, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
