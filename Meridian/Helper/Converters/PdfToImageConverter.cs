﻿using Meridian.Control.Helper.Thumbnail;
using Meridian.Helper.Handlers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace Meridian.Helper.Converters
{
    public class PdfToImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var IsDisconnected = new DisconnectedHandler().IsItemDisconnected(value);
            if (IsDisconnected)
                return null;

            //Call Pdf2Thumb Convertion (Required by pdf thumb view in datagrid)
            var filePath = (string)value;

            if (string.IsNullOrEmpty(filePath))
                return null;

            var selectedItemExt = new FileInfo(filePath).Extension;
            var fileOutPath = "";
            object imageSource = null;

            if (File.Exists(filePath) && selectedItemExt == ".pdf")
                fileOutPath = new FileManagement().CreateThumbnail(filePath);

            //if thumbnail creation succeded convert new image path to image source
            if (!String.IsNullOrEmpty(fileOutPath))
            {
                imageSource = new PathToSourceConvertor().Convert(fileOutPath, targetType, parameter, culture);
            }

            return imageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
