﻿using Meridian.Helper.Handlers;
using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace Meridian.Helper.Converters
{
    class PathToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (new DisconnectedHandler().IsItemDisconnected(value))
                return null;

            return Path.GetFileName((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    
    }
}
