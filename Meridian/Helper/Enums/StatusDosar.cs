﻿using System.Collections.Generic;

namespace Meridian.Helper.Enums
{
    public enum DosarStatusEnum
    {
        NotAvailable,
        Verificare,
        Respingere,
        Reverificare
    }

    public static class DosarStatus
    {
        public static readonly IDictionary<DosarStatusEnum, string> DosarStatusMap = new Dictionary<DosarStatusEnum, string>
        {
            { DosarStatusEnum.NotAvailable, "N/A" },
            { DosarStatusEnum.Verificare, "Verificat" },
            { DosarStatusEnum.Respingere, "Respins" },
            { DosarStatusEnum.Reverificare, "Reverificat" }
        };
    }

}
