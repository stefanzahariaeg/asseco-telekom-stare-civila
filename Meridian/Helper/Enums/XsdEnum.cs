﻿using Meridian.Models.NewModels.Casatorii;
using Meridian.Models.NewModels.Decese;
using Meridian.Models.NewModels.Nasteri;
using System;
using System.Collections.Generic;
using System.IO;

namespace Meridian.Helper.Enums
{
    public enum XSDEnum
    {
        Nasteri,
        Casatorii,
        Decese
    }

    public static class XsdMapper
    {
        public static readonly IDictionary<XSDEnum, string> XsdPathMap = new Dictionary<XSDEnum, string>
        {
            { XSDEnum.Nasteri, Path.Combine(FileUtils.GetExeFolderPath(), "Nasteri.xsd") },
            { XSDEnum.Casatorii, Path.Combine(FileUtils.GetExeFolderPath(), "Casatorii.xsd") },
            { XSDEnum.Decese, Path.Combine(FileUtils.GetExeFolderPath(), "Decese.xsd") }
        };

        public static readonly IDictionary<XSDEnum, Type> XsdTypeMap = new Dictionary<XSDEnum, Type>
        {
            { XSDEnum.Nasteri, typeof(Nasteri) },
            { XSDEnum.Decese, typeof(Decese) },
            { XSDEnum.Casatorii, typeof(Casatorii) }
        };
    }
}
