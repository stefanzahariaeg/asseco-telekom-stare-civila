﻿using Meridian.Commands;
using Meridian.Views.ModalWindows;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Meridian.Helper.Commands
{
    public class WindowAction
    {
        private ICommand _okBtnCommand;
        private ICommand _cancelBtnCommand;

        public ICommand OkLandingPageCommand =>
            _okBtnCommand ?? (_okBtnCommand = new RelayCommand(
                    x =>
                    {
                        //Let window know that close event was triggered by ok button
                        LandingWindow.OkEventTriggered = true;

                        Window window = GetWindowParent((Button)x);
                        window.DialogResult = true;
                        window.Close();
                    }));

        public ICommand OkButtonCommand =>
            _okBtnCommand ?? (_okBtnCommand = new RelayCommand(
                    x =>
                    {
                        Window window = GetWindowParent((Button)x);
                        window.DialogResult = true;
                        window.Close();
                    }));

        public ICommand CancelButtonCommand =>
            _cancelBtnCommand ?? (_cancelBtnCommand = new RelayCommand(
                    x =>
                    {
                        Window window = GetWindowParent((Button)x);
                        window.DialogResult = false;
                        window.Close();
                    }));

        private Window GetWindowParent(Button x)
        {
            var btnOk = x as Button;
            var parent = VisualTreeHelper.GetParent(btnOk);

            while (parent != null && parent.GetType() != typeof(Window))
                parent = VisualTreeHelper.GetParent(parent);

            var parentWindow = parent as Window;

            return parentWindow;
        }
    }
}
