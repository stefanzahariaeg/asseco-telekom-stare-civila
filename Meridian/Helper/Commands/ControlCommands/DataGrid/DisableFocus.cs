﻿using Meridian.Commands;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Helper.Commands
{
    public class DisableFocus
    {
        private ICommand _disableFocusCommand;

        public ICommand DisableFocusCommand =>
            _disableFocusCommand ?? (_disableFocusCommand = new RelayCommand(
                    x =>
                    {
                        //Check if currently focused element is part of DataGrid
                        if (Keyboard.FocusedElement is DataGrid ||
                            Keyboard.FocusedElement is DataGridCell ||
                            Keyboard.FocusedElement is DataGridRow)
                        {
                            return;
                        }

                        if (x != null)
                        {
                            var dg = x as DataGrid;
                            if (dg != null && dg.SelectedItems != null && dg.SelectedItems.Count > 0)
                            {
                                if (!dg.IsMouseOver)
                                {
                                    dg.UnselectAll();

                                    var btn1 = App.Current.MainWindow.FindName("Btn_Verificare") as Button;
                                    var btn2 = App.Current.MainWindow.FindName("Btn_Respingere") as Button;
                                    var btn3 = App.Current.MainWindow.FindName("Btn_Reverificare") as Button;

                                    btn1.IsEnabled = false;
                                    btn2.IsEnabled = false;
                                    btn3.IsEnabled = false;
                                }
                            }
                        }
                    }));
        
    }
}
