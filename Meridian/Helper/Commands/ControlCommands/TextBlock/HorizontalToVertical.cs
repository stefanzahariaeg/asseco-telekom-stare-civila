﻿using Meridian.Commands;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Meridian.Helper.Commands
{
    public class HorizontalToVertical
    {
        private ICommand _convertToVerticalCommand;

        public ICommand ConvertToVerticalCmd =>
            _convertToVerticalCommand ?? (_convertToVerticalCommand = new RelayCommand(
                    x =>
                    {
                        //Null check
                        if (x is null)
                            return;

                        var tBlock = x as TextBlock;
                        var horizontalText = tBlock.Text;
                        tBlock.Text = "";
                        //tBlock.FontWeight = System.Windows.FontWeight.FromOpenTypeWeight(8);

                        if (horizontalText.Split("\r\n").Length > 1)
                        {
                            tBlock.Text = horizontalText;
                            return;
                        }
                           

                        horizontalText.Select(c => c).ToList().ForEach(c =>
                        {
                            if (c.ToString() == " ")
                            {
                                tBlock.Inlines.Add("\n");
                                //tBlock.Inlines.Add("\n");
                            }
                                                          
                            else
                            {
                                tBlock.Inlines.Add((new Run(c.ToString())));
                                tBlock.Inlines.Add(new LineBreak());
                            }
                                

                        });
                    }));
        
    }
}
