﻿using Meridian.Commands;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Helper.Commands
{
    public class StatusControl
    {
        private ICommand _controlEnableCommand;
        private ICommand _controlDisableCommand;

        public ICommand BtnEnableCommand =>
            _controlEnableCommand ?? (_controlEnableCommand = new RelayCommand(
                x =>
                {
                    var btn1 = App.Current.MainWindow.FindName("Btn_Verificare") as Button;
                    var btn2 = App.Current.MainWindow.FindName("Btn_Respingere") as Button;
                    var btn3 = App.Current.MainWindow.FindName("Btn_Reverificare") as Button;

                    btn1.IsEnabled = true;
                    btn2.IsEnabled = true;
                    btn3.IsEnabled = true;
                }));

        public ICommand BtnDisableCommand =>
            _controlDisableCommand ?? (_controlDisableCommand = new RelayCommand(
                x =>
                {
                    if (x != null)
                    {
                        var uc = x as ICommand;

                    }
                    var btn1 = App.Current.MainWindow.FindName("Btn_Verificare") as Button;
                    var btn2 = App.Current.MainWindow.FindName("Btn_Respingere") as Button;
                    var btn3 = App.Current.MainWindow.FindName("Btn_Reverificare") as Button;

                    btn1.IsEnabled = false;
                    btn2.IsEnabled = false;
                    btn3.IsEnabled = false;
                }));
    }
}
