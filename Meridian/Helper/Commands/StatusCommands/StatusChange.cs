﻿using Meridian.Commands;
using Meridian.Helper.Enums;
using Meridian.Helper.Handlers;
using Meridian.Models.StatusDTO;
using Meridian.Views.ModalWindows;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Meridian.Helper.Commands
{
    public class StatusChange
    {
        private ICommand _statusBtnCommand;
        public ICommand StatusButtonCommand =>
            _statusBtnCommand ?? (_statusBtnCommand = new RelayCommand(
            x =>
                {
                    if (x != null)
                    {                          
                        IInputElement focusedControl = Keyboard.FocusedElement;
                        var fe = FocusManager.GetFocusedElement(Application.Current.MainWindow);

                        if (focusedControl is DataGridCell)
                        {
                            //If action was canceled within the OpenReasonDialog => also cancel action here
                            if (OpenReasonDialog() == false)
                                return;

                            Button btn = (Button)x;
                            var statusType = btn.Content;
                            var enumNewStatus = (DosarStatusEnum)Enum.Parse(typeof(DosarStatusEnum), statusType.ToString());

                            var dgCell = focusedControl as DataGridCell;
                            var objList = GetContextEntry(dgCell) as IList<object>;

                            objList.ToList().ForEach(obj =>
                            {
                                var currentStatus = ReflectorHandler.GetTValue(obj, "Status").ToString();
                                if (currentStatus != statusType.ToString())
                                    ReflectorHandler.SetTValue(ref obj, "Status", enumNewStatus);

                                //Set new xml status
                                var docId = ReflectorHandler.GetTValue(obj, "ID").ToString();
                                SetXmlNewStatus(docId, enumNewStatus, GetTextBoxMotive());
                            });
                        }
                    }
                }));
  
        /// <summary>
        /// Sets new xml status for given document
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="dosarEnumStatus"></param>
        private void SetXmlNewStatus(string docId, DosarStatusEnum dosarEnumStatus, string motiv)
        {
            //Setup xmlPath
            var xmlPath = Path.Combine(FileUtils.GetExeFolderPath(), Constants.FisierStatusDosare);

            if (!new FileInfo(xmlPath).Exists)
                throw new Exception("No file found at path: " + xmlPath);

            //Get mapped string assigned to xml new status
            var xmlNewStatus = DosarStatus.DosarStatusMap[dosarEnumStatus];
            //Get xml current status
            var xmlCurrentStatus = XmlUtils.GetElementValById(xmlPath, docId, "Status");

            var currentDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            if (xmlCurrentStatus != null)
            {
                //Send status change to xml status file
                if (xmlCurrentStatus != xmlNewStatus)
                {
                    XmlUtils.SetElementValById(xmlPath, docId, "Status", xmlNewStatus);
                    XmlUtils.SetElementValById(xmlPath, docId, "Motiv", motiv);
                    XmlUtils.SetElementValById(xmlPath, docId, "Data", currentDate);
                }               
            }
            else
            {
                //Add new entry in status xml
                StatusDocument statusDoc = new StatusDocument
                {
                    ID = docId,
                    Status = xmlNewStatus,
                    Data = currentDate
                };
                XmlUtils.AddElementToXml(xmlPath, statusDoc);
            }
        }

        /// <summary>
        /// Searches for the VM entry corresponding to the currently selected item
        /// </summary>
        /// <param name="dgCell"></param>
        /// <returns>Returns a vm collection item</returns>
        private object GetContextEntry(DataGridCell dgCell)
        {
            var parent = VisualTreeHelper.GetParent(dgCell);

            while (parent != null && parent.GetType() != typeof(DataGrid))
                parent = VisualTreeHelper.GetParent(parent);
       
            var dg = parent as DataGrid;

            return dg.SelectedItems;
        }

        /// <summary>
        /// Searches for TextBox that holds motive 
        /// </summary>
        /// <returns>Returns the TextBox Motive text</returns>
        private string GetTextBoxMotive()
        {
            var modalWindow = StatusWindow.Instance.ModalWindow;

            var tBox = modalWindow.Content as TextBox;

            var template = tBox.Template;
            var myControl = (TextBox)template.FindName("InputTextBox", tBox);

            return myControl.Text;            
        }

        /// <summary>
        /// Opens a modal window representing the Reason Input Dialog
        /// </summary>
        /// <returns>Returns the status of the Reason Dialog</returns>
        private bool? OpenReasonDialog()
        {
            StatusWindow.Instance.SetupModalWindow();
            return StatusWindow.Instance.ModalWindow.ShowDialog();
        }
    }
}
