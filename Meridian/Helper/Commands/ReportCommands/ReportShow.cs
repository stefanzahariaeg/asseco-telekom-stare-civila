﻿using Meridian.Commands;
using Meridian.Helper.Enums;
using Meridian.Helper.Extensions;
using Meridian.Helper.FileCaster;
using Meridian.Models.StatusDTO;
using Meridian.Views.ModalWindows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Helper.Commands
{
    public class ReportView
    {
        private ICommand _reportShowBtnCommand;
        public ICommand ReportShowButtonCommand =>
            _reportShowBtnCommand ?? (_reportShowBtnCommand = new RelayCommand(
            x =>
            {       
                //If action was canceled within the OpenCalendarDialog => also cancel action here
                if (OpenCalendarDialog() == false)
                    return;

                var formatInfo = new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" };
                string startDate;
                string endDate;

                startDate = GetDateControlValue("startDateControl");
                endDate = GetDateControlValue("endDateControl");

                if (string.IsNullOrEmpty(startDate) ||
                    string.IsNullOrEmpty(endDate))
                {
                    SetupDateControlDefaultValue();
                    startDate = DateTime.Now.FirstDayOfMonth().ToString("dd/MM/yyyy");
                    endDate = DateTime.Now.LastDayOfMonth().ToString("dd/MM/yyyy");
                }
                else
                {
                    startDate = DateTime.Parse(startDate).ToString("dd/MM/yyyy");
                    endDate = DateTime.Parse(endDate).ToString("dd/MM/yyyy");
                }

                //Generate pdf report
                CreatePdfReport(startDate, endDate);

                //Show pdf report
                ShowReport();
            }));


        /// <summary>
        /// Opens a modal window representing the Report Calendar Input Dialog
        /// </summary>
        /// <returns>Returns the status of the Calendar Dialog</returns>
        private bool? OpenCalendarDialog()
        {
            CalendarWindow.Instance.SetupModalWindow();
            return CalendarWindow.Instance.ModalWindow.ShowDialog();
        }

        /// <summary>
        /// Searches for DatePicker that holds date 
        /// </summary>
        /// <returns>Returns the DatePicker value</returns>
        private string GetDateControlValue(string datePickerName)
        {
            var modalWindow = CalendarWindow.Instance.ModalWindow;
            var dPicker = modalWindow.Content as DatePicker;

            var template = dPicker.Template;
            var myControl = (DatePicker)template.FindName(datePickerName, dPicker);

            return myControl.Text;
        }

        /// <summary>
        /// Sets up control value to : Now
        /// Is used in case user has not provided valid values
        /// </summary>
        private void SetupDateControlDefaultValue()
        {
            //Set default dates on datepickers
            var modalWindow = CalendarWindow.Instance.ModalWindow;
            var dPicker = modalWindow.Content as DatePicker;

            var template = dPicker.Template;
            var startDate = template.FindName("startDateControl", dPicker) as DatePicker;
            var endDate = template.FindName("endDateControl", dPicker) as DatePicker;

            var dateNow = DateTime.Now.Date;
            startDate.SelectedDate = dateNow;
            endDate.SelectedDate = dateNow;
            startDate.DisplayDate = dateNow;
            endDate.DisplayDate = dateNow;
        }

        private void CreatePdfReport(string startDate, string endDate)
        {
            var exePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var fileTxtPath = Path.Combine(exePath, Constants.FisierRaportTxt);
            var fileOutPath = Path.Combine(exePath, Constants.FisierRaportPdf);

            //create .txt report file
            var reportDict = GetReportContent(startDate, endDate);

            //Create text file using report dictionary
            File.WriteAllText(fileTxtPath, "Interval raport:    " + startDate + " - " + endDate +
                Environment.NewLine + Environment.NewLine + Environment.NewLine);

            File.AppendAllLines(fileTxtPath,
                        reportDict.Select(x => "[" + x.Key.ToString() + " " + x.Value + "]").ToArray());

            PdfWriter pdfWriter =
                new PdfWriter(842.0f, 1190.0f, 10.0f, 10.0f);

            if (fileTxtPath.Length > 0)
                pdfWriter.Write(fileTxtPath, fileOutPath);
            
        }

        private Dictionary<DosarStatusEnum, int> GetReportContent(string startDate, string endDate)
        {
            var formatInfo = new DateTimeFormatInfo()
            {
                ShortDatePattern = "dd/MM/yyyy"
            };

            var exePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var statusFilePath = Path.Combine(exePath, Constants.FisierStatusDosare);

            DateTime dateIn = Convert.ToDateTime(startDate, formatInfo);
            DateTime dateOut = Convert.ToDateTime(endDate, formatInfo);

            var statusDto = XmlUtils.DeserializeXMLFileToObject<StatusDocuments>(statusFilePath);

            var docFilterLst = statusDto.Document
                .Where(doc => Convert.ToDateTime(doc.Data, formatInfo) >= dateIn
                            && Convert.ToDateTime(doc.Data, formatInfo) <= dateOut)
                .ToList();

            //Get report dictionary
            var reportDict = DosarStatus.DosarStatusMap
                .ToDictionary(t => t.Key, t => docFilterLst.Count(doc => doc.Status == t.Value));

            return reportDict;
            
        }

        private void ShowReport()
        {
            var exePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var reportPath = Path.Combine(exePath, Constants.FisierRaportPdf);

            //Create temp file         
            var tmpFileOutName = Path.GetFileNameWithoutExtension(reportPath).AppendTimeStamp() + ".pdf";
            var tmpFileOutPath = Path.Combine(Path.GetTempPath(), tmpFileOutName);

            //Copy original in temp location
            File.Copy(reportPath, tmpFileOutPath);

            var browserChild = new WebBrowser();
            browserChild.Navigate("file:///" + tmpFileOutPath);

            var wind = new Window
            {
                Width = 1200,
                Height = 800,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                Owner = Application.Current.MainWindow,
                Content = browserChild
            };

            wind.ShowDialog();
        }
    }
}
