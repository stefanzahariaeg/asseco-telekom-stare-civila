﻿using Meridian.ViewModels;
using Meridian.Views.UserControls;
using System.Collections;
using System.Collections.Generic;

namespace Meridian.Helper.Extensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Count IEnumerable elements and update UI Total
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int CustomCount<TSource>(this IEnumerable<TSource> source)
        {
            if (source == null)
            {
                //throw Error.ArgumentNull("source");
            }
            ICollection<TSource> is2 = source as ICollection<TSource>;
            if (is2 != null)
            {
                return is2.Count;
            }
            ICollection is3 = source as ICollection;
            if (is3 != null)
            {
                return is3.Count;
            }
            int num = 0;
            using (IEnumerator<TSource> enumerator = source.GetEnumerator())
            {
                var rootPath = UC_TreeExplorer.RootPathFolder;
                while (enumerator.MoveNext() && rootPath == UC_TreeExplorer.RootPathFolder)
                {
                    num++;

                    if (num%1000 == 0)
                    {
                        VM_Casatorii.Default.totalItems = num;
                        VM_Casatorii.Default.RefreshTotalItems();
                    }
                }
            }
            return num;
        }
    }
}
