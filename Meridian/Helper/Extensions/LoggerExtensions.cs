﻿using NLog;
using System;
using System.Windows;

namespace Meridian.Helper.Extensions
{
    public static class LoggerExtensions
    {
        public static void Exit(this Logger logger, string exitMessage)
        {
            logger.Error(exitMessage);
            MessageBox.Show(exitMessage);
            Application.Current.Shutdown();
            Environment.Exit(0);
        }
    }
}
