﻿using Microsoft.Xaml.Behaviors;
using System.Windows;
using System.Windows.Controls;

namespace Meridian.Helper.Behaviours
{
    public class EnableOnGetFocusBehavior : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            AssociatedObject.SelectedCellsChanged += AssociatedObject_GotFocus;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectedCellsChanged -= AssociatedObject_GotFocus;
            base.OnDetaching();
        }

        private void AssociatedObject_GotFocus(object sender, SelectedCellsChangedEventArgs e)
        {
            var dg = sender as DataGrid;
            if (dg != null)
            {              
                if (dg.SelectedItems != null && dg.SelectedItems.Count > 0)
                {
                    var btn1 = App.Current.MainWindow.FindName("Btn_Verificare") as Button;
                    var btn2 = App.Current.MainWindow.FindName("Btn_Respingere") as Button;
                    var btn3 = App.Current.MainWindow.FindName("Btn_Reverificare") as Button;

                    btn1.IsEnabled = true;
                    btn2.IsEnabled = true;
                    btn3.IsEnabled = true;
                }          
            }
        }
    }
}
