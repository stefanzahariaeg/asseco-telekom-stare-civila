﻿using Microsoft.Xaml.Behaviors;
using System.Windows;
using System.Windows.Controls;

namespace Meridian.Helper.Behaviours
{
    public class DisableOnLoseFocusBehavior : Behavior<TabControl>
    {
        protected override void OnAttached()
        {
            AssociatedObject.SelectionChanged += AssociatedObject_LostFocus;

            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= AssociatedObject_LostFocus;

            base.OnDetaching();
        }

        private void AssociatedObject_LostFocus(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource is TabControl)
            {
                var dgCasatorii = Views.UserControls.UC_Casatorii.Instance.FindName("DG_Casatorii") as DataGrid;
                var dgNasteri = Views.UserControls.UC_Nasteri.Instance.FindName("DG_Nasteri") as DataGrid;
                var dgDecese = Views.UserControls.UC_Decese.Instance.FindName("DG_Decese") as DataGrid;

                UnselectDataGrid(dgCasatorii);
                UnselectDataGrid(dgNasteri);
                UnselectDataGrid(dgDecese);
            }         
        }

        private void UnselectDataGrid(DataGrid dataGrid)
        {
            if (dataGrid.SelectedItems != null && dataGrid.SelectedItems.Count > 0)
            {
                if (!dataGrid.IsMouseOver)
                {
                    dataGrid.UnselectAll();

                    var btn1 = App.Current.MainWindow.FindName("Btn_Verificare") as Button;
                    var btn2 = App.Current.MainWindow.FindName("Btn_Respingere") as Button;
                    var btn3 = App.Current.MainWindow.FindName("Btn_Reverificare") as Button;

                    btn1.IsEnabled = false;
                    btn2.IsEnabled = false;
                    btn3.IsEnabled = false;
                }
            }
        }
    }
}
