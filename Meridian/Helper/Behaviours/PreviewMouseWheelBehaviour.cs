﻿using Meridian.Helper.Handlers.VisualRelationship;
using Microsoft.Xaml.Behaviors;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Meridian.Helper.Behaviours
{
    public class PreviewMouseWheelBehaviour : Behavior<DataGrid>
    {
        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseWheel += AssociatedObject_MouseWheel;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseWheel -= AssociatedObject_MouseWheel;
            base.OnDetaching();
        }

        private void AssociatedObject_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!e.Handled)
            {

                var grid = sender as DataGrid;
                var viewer = new XamlExplorer().FindVisualChild<ScrollViewer>(grid);

                if (viewer != null)
                {
                    // We listen on changes to the ScrollViewer's scroll offset; if that changes
                    // we can consider our event handled. In case the ScrollChanged event is never
                    // raised, we take this to mean that we are at the top/bottom of our scroll viewer,
                    // in which case we provide the event to our parent.
                    ScrollChangedEventHandler handler = (senderScroll, eScroll) =>
                        e.Handled = true;

                    viewer.ScrollChanged += handler;
                    // forced layout update is necessary to ensure that the event is called
                    // immediately (as opposed to after some small delay).
                    double oldOffset = viewer.VerticalOffset;
                    double offsetDelta = e.Delta > 0 ? -350 : 350;
                    viewer.ScrollToVerticalOffset(oldOffset + offsetDelta);
                    viewer.UpdateLayout();
                    viewer.ScrollChanged -= handler;
                }

                if (e.Handled) return;
                e.Handled = true;
                var eventArg =
                    new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
                    {
                        RoutedEvent = UIElement.MouseWheelEvent,
                        Source = sender
                    };
               
                var parent = grid.Parent as UIElement;
                parent?.RaiseEvent(eventArg);
            }
        }
    }
}
