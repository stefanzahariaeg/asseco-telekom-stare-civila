﻿using Microsoft.Xaml.Behaviors;
using System.Windows.Controls;

namespace Meridian.Helper.Behaviours
{
    public class ImageOnClickBehaviour : Behavior<Image>
    {
        protected override void OnAttached()
        {
            AssociatedObject.MouseDown += AssociatedObject_MouseDown; ;
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseDown -= AssociatedObject_MouseDown;
            base.OnDetaching();
        }

        private void AssociatedObject_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            throw new System.NotImplementedException();
        }
    }
}
