﻿using System.IO;

namespace Meridian.Helper
{
    public static class ConfigManager
    {
        private static readonly string _configPath;
        static ConfigManager()
        {
            _configPath = Path.Combine(FileUtils.GetExeFolderPath(), "Config.xml");
        }
        public static string LandPageTitle =>
            XmlUtils.GetElementVal(_configPath, "AppTitle");

        public static string RootFolderName =>
            XmlUtils.GetElementVal(_configPath, "RootFolderName");
    }
}
