﻿using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using NLog;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace Meridian.Helper.FileCaster
{
    public class PdfToThumb
    {
        Logger _log = LogManager.GetCurrentClassLogger();

        public Image Cast(string pdfFilePath)
        {
            try
            {
                //Dll handler
                var exePath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                int desired_x_dpi = 10;
                int desired_y_dpi = 10;

                GhostscriptVersionInfo gvi;

                if (Environment.Is64BitOperatingSystem)
                    gvi = new GhostscriptVersionInfo(Path.Combine(exePath, "gsdll64.dll"));
                else
                    gvi = new GhostscriptVersionInfo(Path.Combine(exePath, "gsdll32.dll"));


                using GhostscriptRasterizer rasterizer = new GhostscriptRasterizer();
                rasterizer.CustomSwitches.Add("-dUseCropBox");
                rasterizer.CustomSwitches.Add("-c");
                rasterizer.CustomSwitches.Add("[/CropBox [24 72 559 794] /PAGES pdfmark");
                rasterizer.CustomSwitches.Add("-f");
                rasterizer.CustomSwitches.Add("-r800x600");

                //Move pdf to temp location - get rid of diacritics
                MovePdfFileToTemp(pdfFilePath);

                //Rasterize temp file to thumbnail
                rasterizer.Open(GetTempFilePath(pdfFilePath), gvi, false);
                var pageNumber = 1;
                var nrOfPages = rasterizer.PageCount;

                Image img = rasterizer.GetPage(desired_x_dpi, desired_y_dpi, pageNumber);
                return img;
            }
            catch (Exception)
            {
                Application.Current.Dispatcher
                    .BeginInvoke
                    (
                        DispatcherPriority.Normal, 
                        new Action(() => MessageBox.Show("Failed to create thumbnail for pdf file: " + pdfFilePath))
                    );
                return null;
            }
        }

        static void MovePdfFileToTemp(string pdfFilePath)
        {
            var tmpFilePath = GetTempFilePath(pdfFilePath);
            if (File.Exists(tmpFilePath))
                File.Delete(tmpFilePath);

            File.Copy(pdfFilePath, tmpFilePath);
        }

        static string GetTempFilePath(string pdfFilePath)
        {
            var tmpPath = FileUtils.GetTempPath(pdfFilePath);
            var tmpPathWithoutAccents = FileUtils.RemoveAccents(tmpPath);
            var filePathName = new FileInfo(pdfFilePath).Name;
            var tmpFilePathName = FileUtils.RemoveAccents(filePathName);

            var tmpFilePath = Path.Combine(tmpPathWithoutAccents, tmpFilePathName);

            return tmpFilePath;
        }
    }
}
