﻿using Meridian.Helper.Enums;
using Meridian.Helper.Handlers;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.Xml.Linq;
using Meridian.Models.StatusDTO;
using System.Xml.Schema;
using NLog;
using System.Windows;
using Meridian.Helper.Extensions;

namespace Meridian.Helper
{
    public static class XmlUtils
    {
        static Logger _log = LogManager.GetCurrentClassLogger();

        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            if (string.IsNullOrEmpty(XmlFilename)) return default;

            T returnObject;
            try
            {        
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);         
            }
            catch (Exception exp)
            {
                _log.Error(exp.Message);
                throw exp;
            }
            return returnObject;
        }

        public static bool IsValidXml(string xmlFilePath, string xsdFilePath)
        {
            XmlSchemaSet schema = new XmlSchemaSet();
            schema.Add("", xsdFilePath);

            var doc = XDocument.Load(xmlFilePath);

            bool validationErrors = false;

            doc.Validate(schema, (s, e) =>
            {
                validationErrors = true;
            });

            if (validationErrors)
                return false;

            return true;
        }

        public static bool IsDeserializable<T>(string filePath)
        {
            try
            {
                var dosarDto = DeserializeXMLFileToObject<T>(filePath);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns deserialized collection corresponding to given xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static ObservableCollection<T> InjectXMLCollection<T>(string filePath)
        {
            var dosarCollection = new ObservableCollection<T>();

            var selectedItemExt = new FileInfo(filePath).Extension;
            if (selectedItemExt == ".xml")
            {
                if (IsDeserializable<T>(filePath))
                {
                    var dosarDto = DeserializeXMLFileToObject<T>(filePath);

                    PathHandler.SetupXmlPaths<T>(ref dosarDto, filePath);

                    //Set document status property to dosarDto
                    SetDocumentStatus(ref dosarDto);

                    //Add dosarDto to observable collection
                    dosarCollection.Add(dosarDto);
                }
            }

            return dosarCollection;
        }

        private static void SetDocumentStatus<T>(ref T dosarDto)
        { 
            var pathStatusFile = Path.Combine(FileUtils.GetExeFolderPath(), Constants.FisierStatusDosare);

            if (!new FileInfo(pathStatusFile).Exists)
                throw new Exception("No file found at path: " + pathStatusFile);

            var documentId = ReflectorHandler.GetTValue(dosarDto, "ID").ToString();
            var statusElem = GetElementValById(pathStatusFile, documentId, "Status");

            if (statusElem != null)
            {
                var statusEnumMap = DosarStatus
                    .DosarStatusMap
                    .FirstOrDefault(status => status.Value.Equals(statusElem))
                    .Key;

                ReflectorHandler.SetTValue(ref dosarDto, "Status", statusEnumMap);
            }
            else
                ReflectorHandler.SetTValue(ref dosarDto, "Status", DosarStatusEnum.NotAvailable);
        }

        #region XmlFile operations
        public static string GetElementVal(string xmlPath, string elementName)
        {
            try
            {
                if (!new FileInfo(xmlPath).Exists)
                    throw new Exception("No file found at path: " + xmlPath);

                XDocument xDoc = XDocument.Load(xmlPath);

                return xDoc.Root.Element(elementName).Value;
            }
            catch(Exception)
            {
                var errMsg = "Failed to return Config.xml value for property: " + elementName;
                _log.Exit(errMsg);
                throw new Exception(errMsg);
            }

        }

        /// <summary>
        /// Gets value of element by xmlElement Id
        /// </summary>
        /// <param name="elemId"></param>
        /// <param name="elementName"></param>
        /// <returns>Returns value of element by xmlElement Id</returns>
        public static string GetElementValById(string xmlPath, string elemId, string elementName)
        {
            if (!new FileInfo(xmlPath).Exists)
                throw new Exception("No file found at path: " + xmlPath);

            XDocument xDoc = XDocument.Load(xmlPath);

            var qElem = xDoc.Root.Descendants("document")
                .Where(c => c.Attribute("ID").Value == elemId)
                .FirstOrDefault();

            if (qElem != null)
                return qElem.Element(elementName).Value;

            return null;
        }

        /// <summary>
        /// Set XmlElement Value by Id
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="elemId"></param>
        /// <param name="elementName"></param>
        /// <param name="elementValue"></param>
        public static void SetElementValById(string xmlPath, string elemId,
            string elementName, string elementValue)
        {
            if (!new FileInfo(xmlPath).Exists)
                throw new Exception("No file found at path: " + xmlPath);

            XDocument xDoc = XDocument.Load(xmlPath);

            var qElem = xDoc.Root.Descendants("document")
                .Where(c => c.Attribute("ID").Value == elemId)
                .FirstOrDefault();

            if (qElem != null)
                qElem.Element(elementName).Value = elementValue;

            xDoc.Save(xmlPath);
        }

        /// <summary>
        /// Add new entry to Xml
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="statusDTO"></param>
        public static void AddElementToXml(string xmlPath, StatusDocument statusDTO)
        {
            if (!new FileInfo(xmlPath).Exists)
                throw new Exception("No file found at path: " + xmlPath);

            XDocument xDoc = XDocument.Load(xmlPath);
            XElement elem = new XElement("document");
            elem.Add(new XAttribute("ID", statusDTO.ID));
            elem.Add(new XElement("Status", statusDTO.Status));
            elem.Add(new XElement("Data", statusDTO.Data));
            elem.Add(new XElement("Motiv", statusDTO.Motiv));
            xDoc.Root.Add(elem);
            xDoc.Save(xmlPath);
        }
        #endregion
    }
}
