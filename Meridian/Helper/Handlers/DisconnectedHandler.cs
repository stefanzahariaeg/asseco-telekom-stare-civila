﻿namespace Meridian.Helper.Handlers
{
    public class DisconnectedHandler
    {
        /// <summary>
        /// Method to treat wpf issue related to IsDisconnected Item 
        /// //https://github.com/dotnet/wpf/issues/1706
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool IsItemDisconnected(object item)
        {
            if (item is null)
                return true;

            bool isDisconnected = false;

            var itemType = item.GetType();
            if (itemType.FullName.Equals("MS.Internal.NamedObject"))
            {
                isDisconnected = true;
            }

            return isDisconnected;
        }
    }
}
