﻿using System.Windows;
using System.Windows.Media;

namespace Meridian.Helper.Handlers.VisualRelationship
{
    public class XamlExplorer
    {
        /// <summary>
        /// Finds the first child of a given type in a given <see cref="DependencyObject"/>.
        /// </summary>
        /// <typeparam name="T">The type of the child to search for.</typeparam>
        /// <param name="depObj">The object whose children we are interested in.</param>
        /// <returns>The child object.</returns>
        public T FindVisualChild<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                var visualChild = child as T;
                if (visualChild != null) return visualChild;

                var childItem = FindVisualChild<T>(child);
                if (childItem != null) return childItem;
            }
            return null;
        }
    }
}
