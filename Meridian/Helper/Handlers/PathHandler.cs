﻿using System.IO;
using System.Reflection;

namespace Meridian.Helper.Handlers
{
    public static class PathHandler
    {
        private static void SetupXmlProps<T>(ref T dosarDto, string xmlPath, string tProp)
        {
            var propPath = (string)ReflectorHandler.GetTValue(dosarDto, tProp);

            if (!string.IsNullOrEmpty(propPath))
            {
                var isFile = new FileInfo(propPath).Exists;

                if (!isFile)
                {
                    var rootDir = new FileInfo(xmlPath).Directory.FullName;
                    var fPath = Path.Combine(rootDir, propPath);

                    isFile = new FileInfo(fPath).Exists;

                    if (isFile)
                        ReflectorHandler.SetTValue(ref dosarDto, tProp, fPath);
                }
            }

        }

        public static void SetupXmlPaths<T>(ref T dosarDto, string xmlPath)
        {
            SetupXmlProps(ref dosarDto, xmlPath, "InfoDigitizare.Jpg");
            SetupXmlProps(ref dosarDto, xmlPath, "InfoDigitizare.Pdf");
        }
    }
}
