Meridian app features:

Table structure:

1 row 2 columns

| TreeView | DataTable |
| :---: | :---: | :---: |
| root | Col1 | Col2 |
| :---: | :---: | :---: |
| layer1_1 |  |  |
| :---: | :---: | :---: |
| layer1_2 |  |  |

1) TreeView

- TreeView is placed on the left side of the screen
- It represents a visual tree of the Meridian "Dosare" folder structure

* Clicking a layer in the TreeView renders all corresponding xml files inside that layer
in a DataGrid/DataTable object on the second column 

* Clicking a layer also focuses the selected category in the datagrid


2) DataGrid/DataTable

- DataGrid is placed on the right side of the sreen
- It renders the xml content when selecting a layer(folder) on the TreeView

The datagrid holds 3 categories: 
- Casatorii
- Nasteri
- Decese

Each of those categories holds another 3 subcategories:
- 1921-1960
- 1961-1979
- 1980-2020

=> If you select in the TreeView: "Casatorii -> 1921-1960" the DataGrid is rended with data
      from all of the xml's found under that category selection
	  
For the fields: "Jpg" and "Pdf" thumbnails from the paths provided in the xml's are loaded into the datagrid

- Once you click on a thumbnail a window pops up loading the content of your selection
- You can close the window either by clicking on the 'X' buttom of the new window or by defocusing(clicking outside)
      of the window  